import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { AuthService } from 'src/app/modules/auth/auth.service';
import { ApiService } from '../service/api.service';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  userLoggedIn: boolean = false;
  froalaElements: any;
  public froalaEditors: any;
  public isEditMode: boolean = false;
  // message: string;
  constructor(public translate: TranslateService, private route: Router, private authService:AuthService, private apiService:ApiService) {
    this.froalaElements = {
      headerLogo: { src: "/assets/img/logo-shiphero.png", class: "" },
      developerBlockHeader: "List Your App in our App Store",
      developerBlockSubtext:
        "Register as an app developer and submit your app easily with our App Store Developer Portal",
      developerBlockButton: { innerHTML: "Get Started as an App Developer" },
      developerBlockImage: { src: "/assets/img/dev-block.png", class: "" },
      footerLogo: { src: "/assets/img/logo-shiphero.png", class: "" }
    };
    this.froalaEditors = {
      headerLogo: {
        angularIgnoreAttrs: [
          "ng-reflect-froala-editor",
          "ng-reflect-froala-model"
        ],
        immediateAngularModelUpdate: true,
        imageTextNear: false,
        charCounterCount: true,
        imageEditButtons: [
          "imageReplace",
          "imageSize",
          "imageLink",
          "linkRemove"
        ],
        imageMaxSize: 5242880,
        imageAllowedTypes: ["jpeg", "jpg", "png", "svg"]
      },
      heroHeader: { toolbarInline: true },
      heroSubtext: { toolbarInline: true },
      developerBlockHeader: { toolbarInline: true },
      developerBlockSubtext: { toolbarInline: true },
      developerBlockButton: null,
      developerBlockImage: {
        angularIgnoreAttrs: [
          "ng-reflect-froala-editor",
          "ng-reflect-froala-model"
        ],
        immediateAngularModelUpdate: true,
        imageTextNear: false,
        charCounterCount: true,
        imageEditButtons: [
          "imageReplace",
          "imageSize",
          "imageLink",
          "linkRemove"
        ],
        imageMaxSize: 5242880,
        imageAllowedTypes: ["jpeg", "jpg", "png", "svg"]
      },
      footerLogo: {
        angularIgnoreAttrs: [
          "ng-reflect-froala-editor",
          "ng-reflect-froala-model"
        ],
        immediateAngularModelUpdate: true,
        imageTextNear: false,
        charCounterCount: true,
        imageEditButtons: [
          "imageReplace",
          "imageSize",
          "imageLink",
          "linkRemove"
        ],
        imageMaxSize: 5242880,
        imageAllowedTypes: ["jpeg", "jpg", "png", "svg"]
      }
    };

    this.apiService.getData().subscribe(data=>{
      console.log("Inside get data in header component", data)
      this.isEditMode = data;
      if(this.isEditMode === true){
        console.log("Insie if")
    this.sendUpdatedFroalaElements();

      }
      else{
        console.log("Iside else")
      }

      console.log("===================", data)

      // Do whatever you want with your data
  })
    translate.setDefaultLang("en");
    this.authService.getUserLoginInfo().subscribe(res=>{
      // console.log("res", res)
      if(res){
        this.userLoggedIn = true;
      }
      else{
        this.userLoggedIn = false
      }
    })
  }

public getEditorByName(name: string): any {
    return this.froalaEditors[name];
  }
    // Method to change the language.
  switchLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
  }
  sendUpdatedFroalaElements() {
    this.apiService.updateFroalaElement(this.froalaElements);
  }
  goToHome() {
    this.route.navigate(["/"]);
  }

  toLogin(){
    this.route.navigate(["/login"]);
  }

  toSignup(){
    this.route.navigate(["/signup"]);
  }
  logout(){
    this.authService.signOut();
  }
  toUpdateProfile(){
    this.route.navigate(["/user-profile"]);
  }
}

