import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../auth/auth.service";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  IsInvalid: boolean = false;
  errorMessage: string;
  isPasswordShow: boolean;

  constructor(private router: Router, private auth: AuthService) {
    this.isPasswordShow = false;
  }

  ngOnInit() {
    this.formGroup = new FormGroup({
      Email: new FormControl("", [
        Validators.required
        // Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      ]),
      Password: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
      RememberMe: new FormControl("", [
        // Validators.required
      ])
    });
  }
  showPassword() {
    this.isPasswordShow = !this.isPasswordShow;
  }
  onSubmit() {
    console.log(this.formGroup.value);
    const email = this.formGroup.value.Email,
      password = this.formGroup.value.Password;
    this.auth.signIn(email, password).subscribe(
      result => {
        console.log("result", result);
        this.router.navigate(["/"]);
      },
      error => {
        this.IsInvalid = true;
        this.errorMessage = error.message;
        console.log(error);
      }
    );
  }
}
